"""
The module contains function which are commonly used for
motion equation tests in module testing_of_motion_equation
"""

import math
from dynamics_vector import Vector
from dynamics_of_point_particle import MotionEquationOfParticle
from dynamics_of_point_particle import ForceSource


def prnt(to_print_header, value, name, size, dec, prnt_type):
    """
    The function is used to print set values in a table with a header.
    to_print_header - defines to print the value name in the table header
      or print the value
    name - the value name which printed in the table header
      in respective column
    size - amount of characters in printed value or name
    dec - precision of printed value
    type - type of printed value according to format() method specification
    """
    if to_print_header:
        form = "{:>"+str(size)+"s}"
        print(form.format(str(name)), end="")
    else:
        if value == 0. or abs(value) < 10.**(size-dec-2):
            form = "{: "+str(size)+"."+str(dec)+prnt_type+"}"
            print(form.format(float(value)), end="")
        else:
            power = int(math.log10(value))
            prt_value = float(value)/10.**power
            print("{: .2f}e{:d}".format(prt_value, power), end="")


class FunctionalForce(ForceSource):
    """
    Class presents a force source with force, coordinates of which
    are computed according to give functions.
    The function pointers are passed during the FunctionalForce object
    initialization. There are three separated function for computation of
    each force coordinate.
    """
    def __init__(self,
                 function_x=lambda t, p, v: 0.,
                 function_y=lambda t, p, v: 0.,
                 function_z=lambda t, p, v: 0.):
        """
        function_x - a function of x-coordinate force vector computation
        function_y - a function of y-coordinate force vector computation
        function_z - a function of z-coordinate force vector computation
        The functions must have three parameters:
        - float value of current time in sec.;
        - Vector object of point particle position in inertial coord. system;
        - Vector object of point particle velocity in inertial coord. system;
        """
        ForceSource.__init__(self)
        self.set_function(function_x, function_y, function_z)

    def set_function(self, function_x, function_y, function_z):
        """
        Set function pointers to the attributes.
        For parameters descriptions see docstring for the __init__
        """
        ForceSource.__init__(self)
        self.function_x = function_x
        self.function_y = function_y
        self.function_z = function_z

    def update(self, state_parameters):
        """overridden method of force coordinates computation"""
        self.force.x = self.function_x(state_parameters.time,
                                       state_parameters.position,
                                       state_parameters.velocity)
        self.force.y = self.function_y(state_parameters.time,
                                       state_parameters.position,
                                       state_parameters.velocity)
        self.force.z = self.function_z(state_parameters.time,
                                       state_parameters.position,
                                       state_parameters.velocity)


def test_motion_equation_solution(
        initial_state,  # Initial state parameter
        end_time,  # Time, which corresponds to the solutions [sec.]
        pos_expect,  # Function of expected position vector calculation
        vel_expect,  # Function of expected velocity vector calculation
        takts,  # list of tested takts
        ):
    """
    The function performs testing of motion equation solution,
    presents the results in table form with error relatively expected
    results. The results of the equation solution are computed position
    vector and computed velocity vector atn particle time end_time (float)
    at initial conditions represented by initial_state
    (StateParametersOfParticle). Expected position vector and velocity vector
    are computed by functions pos_expect, vel_expect. The functions have two
    parameter: particle time (float [sec]) and initial state parameter as an
    object StateParametersOfParticle
    """

    # Collection of calculated position and velocity errors
    # respectively to a takt value: key - takt; value - error tuple:
    # [0] - position; [1] -velocity.
    # The variable is going to be returned
    errors = dict()

    # Initializing of motion equation objects with different takts
    equations = [MotionEquationOfParticle(takt) for takt in takts]
    # Coping state parameters for ech equation object
    params = [initial_state.copy() for _ in equations]

    print("  Initial position:", initial_state.position)
    print("  Initial velocity:", initial_state.velocity)

    header = True  # to print a table header
    for i, equation in enumerate(equations):
        # Solving of motion equation for each tact and print out the result
        # in comparison with expected results.
        # The results are presented in a table.
        # i == -1 is used to printout the table header

        error_pos = Vector(1.e5, 1.e5, 1.e5)  # variable of position error
        error_vel = Vector(1.e5, 1.e5, 1.e5)  # variable of velocity error

        # solving of motion equation for the specific tact
        equation.run(end_time, params[i])

        # calculating of position error
        error_pos = params[i].position -\
            pos_expect(params[i].time, initial_state)
        # calculating of velocity error
        error_vel = params[i].velocity -\
            vel_expect(params[i].time, initial_state)
        # error collection to return
        errors[equation.takt] = (error_pos, error_vel)

        while True:
            # First two loops are to print header and first table line.
            # Then just one loop to print a table line
            prnt(header, equation.takt, "tact", 7, 3, "f")
            prnt(header, params[i].time, "time", 5, 1, "f")
            print("|", end="")

            # Printout of position results
            prnt(header, params[i].position[0], "posX", 7, 1, "f")
            prnt(header, error_pos[0], "errX", 7, 0, "e")
            prnt(header, params[i].position[1], "posY", 7, 1, "f")
            prnt(header, error_pos[1], "errY", 7, 0, "e")
            prnt(header, params[i].position[2], "posZ", 7, 1, "f")
            prnt(header, error_pos[2], "errZ", 7, 0, "e")
            print("|", end="")

            # Printout of velocity results
            prnt(header, params[i].velocity[0], "velX", 7, 2, "f")
            prnt(header, error_vel[0], "errX", 7, 0, "e")
            prnt(header, params[i].velocity[1], "velY", 7, 2, "f")
            prnt(header, error_vel[1], "errY", 7, 0, "e")
            prnt(header, params[i].velocity[2], "velZ", 7, 2, "f")
            prnt(header, error_vel[2], "errZ", 7, 0, "e")
            print("|", end="")

            print("")  # end line of the table

            if header: # First time repeat loop for first table line
                header = False
            else:
                break

    return errors


def find_max_position_velosity_error(error_dict):
    """
    The function search for a largest absolute value of
    position error and velocity error within a dictionary error_dict, which
    is generated by test_motion_equation_solution()
    """

    # A variable of maximal error of calculated position
    # relatively expected value
    max_pos_error = 0.
    # A variable of maximal error of calculated velocity
    # relatively expected value
    max_vel_error = 0.

    for value in error_dict.values():
        # updating of maximal position error out all coordinates
        for coord in range(3):
            abs_error = abs(value[0][coord])
            if abs_error > max_pos_error:
                max_pos_error = abs_error
        # updating of maximal velocity error out all coordinates
        for coord in range(3):
            abs_error = abs(value[1][coord])
            if abs_error > max_vel_error:
                max_vel_error = abs_error

    return (max_pos_error, max_vel_error)


def print_error_difference_table(
        time,  # current time [sec]
        coef,  # set of coefficients
        takt_set,  # set of takts
        computed_error,  # dictionary of computed errors respectively takt
        theor_error_function  # function of theoretical error computation
        ):
    """
    The function prints table of differences of computed errors and
    theoretical errors for different takt and coefficient
    Function method_error_function must has parameters:
    time; initial state object, takt
    """
    print("  {:>7s}  k={:5.2f}  k={:5.2f}  k={:5.2f}".
          format("takt", coef[0], coef[1], coef[2]))
    max_error = 0.
    for tkt in takt_set:
        error = computed_error[tkt] -\
                theor_error_function(time, tkt)
        print("  {: 7.3f} {: 7.1e} {: 7.1e} {: 7.1e}".
              format(tkt, error[0], error[1], error[2]))
        for i in range(3):
            error_abs = abs(error[i])
            if max_error < error_abs:
                max_error = error_abs
    return max_error
