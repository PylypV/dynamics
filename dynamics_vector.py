"""
The module contains a class Vector, which presents abstraction of
a three-dimensional vector associated with
right-handed orthogonal three-dimensional coordinate system.
The class realizes vector coordinate storage of a vector and
mathematical operations with vectors.
Following mathematical operations are realized in the class Vector:
- sum of two vectors (operators +, +=);
- subtraction of two vectors (operators -, -=);
- comparison of two vectors (operators ==, !=);
- cross-product of two vectors (operator *);
- vector scaling (operator *);
- dot-product of two vectors (operator &);
- element-wise multiplication of two vectors (operator |);
- reversing of a vector (unary minus operator -);
- getting length of a vector (unary operator ~).
"""


import math


class Array():
    """
    The class Array organizes a storage of tree dimensional vector coordinates,
    realizes indexing to the coordinates,
    realizes useful string representation of the vector coordinates.
    This class operates directly with the coordinate storage.
    """
    def __init__(self, x, y, z):
        """This realization represents a vector as a list of coordinates"""
        if isinstance(x, Array):
            # Initialization by another vector,
            # similar to copy of the initial object
            self.__array = [x[0], x[1], x[2]]
            return
        self.__array = [float(x), float(y), float(z)]

    def __str__(self):
        return str(self.__array)

    def __repr__(self):
        return str(self.__array)

    def __getitem__(self, index):
        return self.__array[index]

    def __setitem__(self, index, value):
        self.__array[index] = float(value)
        return self.__array[index]


class Vector(Array):
    """
    The class Vector represents naming of coordinates and
    three-dimensional vector operations
    """
    def __init__(self, x=0., y=0., z=0.):
        Array.__init__(self, x, y, z)

    x = property()  # First vector coordinate
    y = property()  # Second vector coordinate
    z = property()  # Third vector coordinate

    @x.getter
    def x(self):
        """Getting x-coordinate of the vector"""
        return self[0]

    @x.setter
    def x(self, value):
        """Setting x-coordinate of the vector"""
        self[0] = float(value)
        return self[0]

    @y.getter
    def y(self):
        """Getting y-coordinate of the vector"""
        return self[1]

    @y.setter
    def y(self, value):
        """Setting y-coordinate of the vector"""
        self[1] = float(value)
        return self[1]

    @z.getter
    def z(self):
        """Getting z-coordinate of the vector"""
        return self[2]

    @z.setter
    def z(self, value):
        """Setting z-coordinate of the vector"""
        self[2] = float(value)
        return self[2]

    def __eq__(self, right):
        """Comparison for equality of two vectors by their coordinates"""
        if not isinstance(right, Vector):
            raise TypeError("a Vector object is comparable only "
                            "with a Vector  object")
        return (self[0] == right[0] and
                self[1] == right[1] and
                self[2] == right[2])

    def __ne__(self, right):
        """Comparison for non-equality of two vectors by their coordinates"""
        if not isinstance(right, Vector):
            raise TypeError("a Vector object is comparable only with "
                            "a Vector  object")
        return (self[0] != right[0] or
                self[1] != right[1] or
                self[2] != right[2])

    def __neg__(self):
        """
        Unary operator
        returns a vector with same length and with reverse direction
        """
        return Vector(-self[0], -self[1], -self[2])

    def __invert__(self):
        """Unary operator ~ returns length of the vector"""
        return math.sqrt(self[0]*self[0] + self[1]*self[1] + self[2]*self[2])

    def __add__(self, right):
        """Sum of two vectors"""
        if isinstance(right, Vector):
            return Vector(self[0]+right[0], self[1]+right[1], self[2]+right[2])
        raise TypeError("right operand of Vector sum operator (+) "
                        "must be a vector")

    def __iadd__(self, right):
        """Adding to the vector another vector"""
        if isinstance(right, Vector):
            # self[0] += right[0]
            # self[1] += right[1]
            # self[2] += right[2]
            # return self
            return Vector(self[0]+right[0], self[1]+right[1], self[2]+right[2])
        raise TypeError("right operand of Vector adding operator (+=) "
                        "must be a vector")

    def __sub__(self, right):
        """Difference (subtraction) of two vectors"""
        if isinstance(right, Vector):
            return Vector(self[0]-right[0], self[1]-right[1], self[2]-right[2])
        raise TypeError("right operand of Vector subtraction operator (-) "
                        "must be a vector")

    def __isub__(self, right):
        """Subtraction another vector from the vector"""
        if isinstance(right, Vector):
            # self[0] -= right[0]
            # self[1] -= right[1]
            # self[2] -= right[2]
            # return self
            return Vector(self[0]-right[0], self[1]-right[1], self[2]-right[2])
        raise TypeError("right operand of Vector subtraction "
                        "operator (-=) must be a vector")

    def __mul__(self, right):
        """Cross-product of two vectors or scaling"""
        if isinstance(right, Vector):
            return Vector(self[1]*right[2] - self[2]*right[1],
                          self[2]*right[0] - self[0]*right[2],
                          self[0]*right[1] - self[1]*right[0])
        scale = float(right)
        return Vector(scale*self[0], scale*self[1], scale*self[2])

    def __and__(self, right):
        """Dot-product of two vectors"""
        if isinstance(right, Vector):
            return self[0]*right[0] + self[1]*right[1] + self[2]*right[2]
        raise TypeError("right operand of dot-product operator (&) \
                         must be a vector")

    def __or__(self, right):
        """Element-wise multiplication of two vectors"""
        if isinstance(right, Vector):
            return Vector(self[0]*right[0], self[1]*right[1], self[2]*right[2])
        raise TypeError("right operand of element-wise \
                         multiplication operator (&) must be a vector")

    def __truediv__(self, right):
        """Division of each vector coordinate on a float value"""
        scalar = float(right)
        return Vector(self[0]/scalar, self[1]/scalar, self[2]/scalar)
