"""
The module contains testing functions of class StateParametersOfParticle
which represents state parameters of a point particle.
There are following test procedures:
- Testing of state parameters initialisation.
- Testing of force source collection operation.
- Testing of coping of a StateParametersOfParticle object.
The function testing_class_state_parameters_of_particle()
runs all of those tests
"""


from dynamics_vector import Vector
from dynamics_of_point_particle import StateParametersOfParticle
from dynamics_of_point_particle import ForceSource


def passed():
    """Marks passed tests"""
    print(" =>PASSED")


def test_mass_getting_setting():
    """testing of operation with mass"""
    parameters = StateParametersOfParticle()
    print("  Getting of a default mass value:", parameters.mass, end="")
    assert parameters.mass == float(1.),\
        "WRONG getting of default mass value"
    passed()
    parameters.mass = 100.1
    print("  Setting of a mass value to 100.1:", parameters.mass, end="")
    assert parameters.mass == 100.1,\
        "WRONG setting/getting of mass value"
    passed()

    print("  Mass setting to negative value ", end="")
    try:
        parameters.mass = -10
        assert False, "Mass setting to negative value must raise "\
                      "an exception ValueError"
    except ValueError:
        print("raise an exception ValueError", end="")
    passed()
    print("  Mass setting to zero value ", end="")
    try:
        parameters.mass = 0.
        assert False, "Mass setting to zero value must raise "\
                      "an exception ValueError"
    except ValueError:
        print("raise an exception ValueError", end="")
    passed()


def testing_state_parameters_initialization():
    """Testing of StateParametersOfParticle object initialization"""
    print("\nTest of StateParametersOfParticle initialization")

    parameters = StateParametersOfParticle()
    print("  Testing default iniitialication and printout")
    print("    ", parameters)
    assert str(parameters) == \
        "T= 0.000 M=  1.0; Pos[0.0, 0.0, 0.0]; Vel[0.0, 0.0, 0.0]; "\
        "Acc[0.0, 0.0, 0.0]; Frc[0.0, 0.0, 0.0]",\
        "WRONG default initialization of StateParametersOfParticle"
    passed()

    test_mass_getting_setting()

    print("  Setting of the total force of the parameter:")
    try:
        parameters.force = Vector(1, 2, 3)
    except AttributeError:
        print("    sends an exception AttributeError", end="")
    passed()

    print("  Access to the vector paraveters except total force:")
    parameters.mass = float(100.2)
    parameters.position = Vector(1, 0, 0)
    parameters.velocity = Vector(0, 10, 0)
    parameters.acceleration = Vector(0, 0, 100)
    print("    ", parameters)
    assert str(parameters) == \
        "T= 0.000 M= 100.2; Pos[1.0, 0.0, 0.0]; Vel[0.0, 10.0, 0.0]; "\
        "Acc[0.0, 0.0, 100.0]; Frc[0.0, 0.0, 0.0]",\
        "WRONG  initialization of StateParametersOfParticle"
    passed()

    print("  Printing of state parameter collection", end="")
    coll = [parameters, StateParametersOfParticle()]
    # print(coll)
    assert str(coll) == ("[\nT= 0.000 "
                         "M= 100.2; Pos[1.0, 0.0, 0.0]; Vel[0.0, 10.0, 0.0]; "
                         "Acc[0.0, 0.0, 100.0]; Frc[0.0, 0.0, 0.0], "
                         "\nT= 0.000 "
                         "M=  1.0; Pos[0.0, 0.0, 0.0]; Vel[0.0, 0.0, 0.0]; "
                         "Acc[0.0, 0.0, 0.0]; Frc[0.0, 0.0, 0.0]]"),\
        "WRONG printing of state parameter collection"
    passed()


def testing_source_collection_operation():
    """Testing of force source collection operation"""
    print("\nTesting of force source collection operation")

    parameters = StateParametersOfParticle()
    print("  Initially the source collection is empty: size",
          parameters.sources_number(), end="")
    assert parameters.sources_number() == 0,\
        "WRONG Initially the source collection must by empty"
    passed()

    class ConstantForce(ForceSource):
        """Class presents a constant force"""
        def __init__(self, constant_force=Vector()):
            ForceSource.__init__(self)
            self.name = "constant_force"
            self.constant_force = constant_force

        def update(self, state_parameters):
            """overridden method of force coordinates computation"""
            self.force = self.constant_force

        def too_few_public_methods(self):
            """to pass a pylint"""
            return 1

    f_1 = ConstantForce(Vector(1, 0, 0))
    f_2 = ConstantForce(Vector(0, 1, 0))
    print("  Including first constant force source:", f_1.constant_force)
    parameters.include_source(f_1)
    print("  Source collection size is", parameters.sources_number(), end="")
    assert parameters.sources_number() == 1,\
        "WRONG first force source is not added"
    passed()
    parameters.update()
    print("  Total force after update is equal to "
          "a first force source", parameters.force, end="")
    assert parameters.force == Vector(1., 0., 0.),\
        "WRONG total force after first update"
    passed()
    print("  Including second constant force source:", f_2.constant_force)
    parameters.include_source(f_2)
    print("  Source collection size is", parameters.sources_number(), end="")
    assert parameters.sources_number() == 2,\
        "WRONG second force source is not added"
    passed()
    parameters.update()
    print("  Total force after update is equal to "
          "sum of source forces", parameters.force, end="")
    assert parameters.force == Vector(1., 1., 0.),\
        "WRONG total force after first update"
    passed()


def testing_state_parameter_object_coping():
    """Testing of correct state object copy"""
    print("\nTesting of correct state object copy")

    class ConstantForce(ForceSource):
        """Class presents a constant force"""
        def __init__(self, constant_force=Vector()):
            ForceSource.__init__(self)
            self.name = "constant_force"
            self.constant_force = constant_force

        def update(self, state_parameters):
            """overridden method of force coordinates computation"""
            self.force = self.constant_force

        def too_few_public_methods(self):
            """to pass a pylint"""
            return 1

    f_1 = ConstantForce(Vector(1, 0, 0))
    f_2 = ConstantForce(Vector(0, 1, 0))
    f_3 = ConstantForce(Vector(0, 0, 9))
    f_4 = ConstantForce(Vector(1, 2, 3))

    par_1 = StateParametersOfParticle()
    id_1 = id(par_1._StateParametersOfParticle__sources)
    par_1.include_source(f_1)
    par_1.include_source(f_2)
    par_1.update()
    id_2 = id(par_1._StateParametersOfParticle__sources)
    print("  ID of source storage does not change after force including",
          end="")
    assert id_1 == id_2, "WRONG the ID of sources storage must not change"
    passed()

    par_2 = par_1.copy()
    print("  IDs of copied object differs from ID of origin: "
          "par_2 = par_1.copy()", end="")
    assert id(par_2) != id(par_1),\
        "WRONG: ID of copied object must differ from ID of an initial object"
    passed()
    print("  IDs of the __sources attribute of copied object"
          " is the same that the origin has:", end="")
    assert id(par_1._StateParametersOfParticle__sources) ==\
           id(par_2._StateParametersOfParticle__sources),\
        "WRONG: ID of attribute __sources of a copied object "\
        "must be the same than the __sources ID of the initial object"
    passed()
    par_1.include_source(f_3)
    par_1.include_source(f_4)
    print("  Adding a force source to the origin object similarly changes\n"
          "    the __sources in copied object. Same number of sources:",
          par_1.sources_number(), par_2.sources_number(), end="")
    assert par_1.sources_number() == par_2.sources_number(),\
        "WRONG copy of state parameter: Adding a force source to "\
        "the origin object similarly must change the __sources " \
        "in copied object."
    passed()
    par_1.update()
    print("  Updating of origin state parameter does not update "
          "a copied parameters", end="")
    assert par_1.force != par_2.force,\
        "WRONG: Updating of origin state parameter must not update"\
        "a copied parameters"
    passed()
    par_2.position = Vector(100., 101., 102.)
    par_2.velocity = Vector(110., 121., 132.)
    par_2.acceleration = Vector(200., 211., 202.)
    par_2.mass = 1001.
    print("  Updating of copied attribute does not change "
          "a correspondent origin attribute", end="")
    assert par_1.position != par_2.position and\
           par_1.velocity != par_2.velocity and\
           par_1.acceleration != par_2.acceleration and\
           par_1.mass != par_2.mass,\
        "WRONG: Updating of copied attribute must not change "\
        "a correspondent origin attribute"
    passed()


def testing_class_state_parameters_of_particle():
    """the function runs all of module tests"""
    print("\n\nTEST OF CLASS StateParametersOfParticle")
    testing_state_parameters_initialization()
    testing_source_collection_operation()
    testing_state_parameter_object_coping()


if __name__ == "__main__":
    testing_class_state_parameters_of_particle()
