"""
The module contains testing functions of class Vector.
There are following test procedures:
- testing of Vector object initialization;
- testing of indexing of Vector coordinsates;
- testing of a coordinate acess;
- testing of comparison operators;
- testing of mathematical operators;
- testing of vector algebraic_properties.
The function testing_of_class_vector() runs all of those tests
"""


from dynamics_vector import Vector


def passed():
    """Marks passed tests"""
    print(" =>PASSED")


def testing_vector_initialization():
    """Testing of Vector object initialization"""

    print("Initialization of Vector object")

    v_0 = Vector()
    print("  Default initialization and print out: Vector() =", v_0, end="")
    assert str(v_0) == str("[0.0, 0.0, 0.0]"), \
        "WRONG default initialization of Vector object or wrong method __str__"
    passed()

    v_0 = Vector(1., 2., 3.)
    print("  Explicit initialization and print out: Vector(1., 2., 3.) =",
          v_0, end="")
    assert str(v_0) == str("[1.0, 2.0, 3.0]"),\
        "WRONG explicit initialization of Vector object or wrong method__str__"
    passed()

    v_init = Vector(1.1, 2.2, 3.3)
    v_0 = Vector(v_init)
    print("  Initialization by other Vector object", end="")
    assert str(v_0) == str(v_init),\
        "WRONG initialization by other Vector object"
    passed()
    print("  IDs of new object and the initial object are different", end="")
    assert id(v_0) != id(v_init),\
        "WRONG ID of new object and the initial object must be different"
    passed()

    v_0 = Vector(1., 2.)
    print("  Explicit initialization by two arguments: "
          "Vector(1., 2.) = ", v_0, end="")
    assert str(v_0) == str("[1.0, 2.0, 0.0]"),\
        "WRONG initialization by one argument"
    passed()

    v_0 = Vector(1.)
    print("  Explicit initialization by one argument: Vector(1.) =", v_0,
          end="")
    assert str(v_0) == str("[1.0, 0.0, 0.0]"),\
        "WRONG initialization by one argument"
    passed()

    v_0 = Vector(1, 2, 3)
    print("  Initialization by integers with conversion to float: "
          "Vector(1, 2, 3) = ", v_0, end="")
    assert str(v_0) == str("[1.0, 2.0, 3.0]"),\
        "WRONG initialization with conversion of integer type"
    passed()

    v_0 = Vector("10", "20.", "1e3")
    print('  Initialization by string, that is convertible to float:\n',
          '    Vector("10", "20.", "1e3") = ', v_0, end="")
    assert str(v_0) == str("[10.0, 20.0, 1000.0]"),\
        "WRONG initialization with conversion of string type"
    passed()

    try:
        v_0 = Vector("10ff", "20.", "1e3")
        assert False,\
            "WRONG: no exception of initialization by non-convertible  string"
    except ValueError:
        print('  Initialization by a string, that is not convertible to float,'
              '\n    sends an exception ValueError: '
              'Vector("10ff", "20.", "1e3") = ValueError', end="")
        passed()

    col = (Vector(1, 2, 3), Vector(4, 3, 5), Vector(6, 7, 8))
    print("  Printing of vector collection:", col, end="")
    assert str(col) ==\
           str(([1.0, 2.0, 3.0], [4.0, 3.0, 5.0], [6.0, 7.0, 8.0])),\
        "WRONG printing of a vector collection"
    passed()


def testing_vector_indexing():
    """Testing of Vector object indexing"""

    print("\nIndexing of Vector object")

    v_0 = Vector(1., 2., 3.)
    print("  Getting an element by index: v_0[0] ={}; v_0[1] ={}; "
          "v_0[2] ={}; ".format(v_0[0], v_0[1], v_0[2]), end="")
    assert v_0[0] == 1. and v_0[1] == 2. and v_0[2] == 3.,\
        "WRONG indexing of vector"
    passed()

    v_0[0] = 1.
    v_0[1] = 1.5
    v_0[2] = 1e2
    print("  Setting an element by index: v_0[0] ={}; v_0[1] ={}; "
          "v_0[2] ={}; ".format(v_0[0], v_0[1], v_0[2]), end="")
    assert v_0[0] == 1. and v_0[1] == 1.5 and v_0[2] == 100.,\
        "WRONG setting of elements by index"
    passed()

    v_0[0] = 1.
    v_0[1] = "1.5"
    v_0[2] = "1e2"
    print('  Setting an element by convertible types:\n',
          '    v_0[0]=1.; v_0[1]="1.5"; v_0[2]= "1e2": ', v_0, end="")
    assert v_0[0] == 1. and v_0[1] == 1.5 and v_0[2] == 100.,\
        "WRONG setting of elements by convertible  type"
    passed()

    try:
        v_0[0] = "10ff"
        v_0[1] = "20,"
        v_0[2] = "1ev3"
        assert False, "WRONG: no exception of element setting by "\
                      "non-convertible  string"
    except ValueError:
        print('  Setting an element by a string, that is not convertible '
              'to float,\n    sends an exception ValueError', end="")
        passed()

    try:
        var_ind = v_0[3]
        print(var_ind)
        assert False, "WRONG: index out of range v_0[3] does not raise "\
                      "an exception"
    except IndexError:
        print("  Getting an element by index out of range v_0[3] sends "
              "an exception IndexError", end="")
        passed()

    try:
        v_0[3] = 100.
        assert False, "WRONG: index out of range v_0[3] does not raise "\
                      "an exception"
    except IndexError:
        print("  Setting an element by index out of range v_0[3] sends "
              "an exception IndexError", end="")
        passed()

def testing_vector_coordinates_access():
    """Testing of access to vector coordinates"""
    print("\nTesting of access to vector coordinates")

    v_0 = Vector()
    v_0.x = 1.1
    v_0.y = 2.2
    v_0.z = 3.3
    print("  Getting and setting of elements by xyz-coordinates:\n"
          "    v_0.x = 1.1; v_0.y=2.2; v_0.z=3.3 :", v_0, end="")
    assert v_0[0] == 1.1 and v_0[1] == 2.2 and v_0[2] == 3.3,\
        "WRONG setting of vector coordinates"
    assert v_0.x == 1.1 and v_0.y == 2.2 and v_0.z == 3.3,\
        "WRONG getting of vector coordinates"
    passed()

    v_0.x = 1
    v_0.y = "1.5"
    v_0.z = "1e2"
    print('  Setting coordinates by convertible types:\n'
          '    v_0.x=1; v_0.y="1.5"; v_0.z= "1e2": ', v_0, end="")
    assert v_0[0] == 1. and v_0[1] == 1.5 and v_0[2] == 100.,\
        "WRONG setting of coordinates by convertible  type"
    assert v_0.x == 1. and v_0.y == 1.5 and v_0.z == 100.,\
        "WRONG getting of coordinates by convertible  type"
    passed()

    try:
        v_0.x = "10ff"
        v_0.y = "20,"
        v_0.z = "1ev3"
        assert False,\
            "WRONG: no exception of coordinate setting "\
            "by non-convertible string"
    except ValueError:
        print('  Setting coordinates by strings, which are not convertible to '
              'float\n    sends an exception ValueError', end="")
        passed()


def testing_vector_comparison_operators():
    """Testing of vector comparison operators"""

    print("\nTesting of vector comparison operators")

    # operator ==
    v_1 = Vector(1., -2., 3.)
    v_2 = Vector(1., -2., 3.)
    print("  Comparison for equality of two equal vectors", v_1, v_2, end="")
    assert v_1 == v_2, "WRONG comparison of two equal vectors"
    passed()
    print("  Comparison for equality of two different Vectors", end="")
    assert not (v_1 == Vector(1.1, -2., 3.) or
                v_1 == Vector(1., -2.2, 3.) or
                v_1 == Vector(1., -2., 3.3)),\
        "WRONG comparison of two different vectors"
    passed()
    print("  Comparison for equality of a vector with a right operand "
          "wrong type")
    try:
        var = (v_1 == 1.1)
        print(var)
        assert False, "WRONG: no exception of comparison with a wrong type"
    except TypeError:
        print("   raises exception TypeError", end="")
        passed()

    # operator !=
    print("  Comparison for non-equality of two equal Vectors", v_1, v_2,
          end="")
    assert v_1 == v_2, "WRONG comparison for non-equality of two equal vectors"
    passed()

    print("  Comparison for non-equality of two different Vectors", end="")
    assert (v_1 != Vector(1.1, -2., 3.) and
            v_1 != Vector(1., -2.2, 3.) and
            v_1 != Vector(1., -2., 3.3)),\
        "WRONG comparison for non-equality of two different vectors"
    passed()
    print("  Comparison for non-equality of a vector with "
          "a right operand wrong type\n", end="")
    try:
        var = (v_1 == 1.1)
        print(var)
        assert False,\
            "WRONG: no exception of comparison for non-equality of a vector "\
            "with a wrong type right operand"
    except TypeError:
        print("   raises exception TypeError", end="")
        passed()


def testing_vector_mathematical_operators():
    """Testing of vector mathematical operators"""

    print("\nTesting of vector mathematical operators")
    testing_operator_invert()
    testing_operator_unarry_minus()
    testing_operator_sum()
    testing_operator_sum_equal()
    testing_operator_substraction()
    testing_operator_substraction_equal()
    testing_operator_multiplication()
    testing_operator_and()
    testing_operator_or()
    testing_vector_division_operator()


def testing_operator_invert():
    """operator ~ returns vector length"""
    v_0 = Vector(3., -4., 3.75)
    print("  Length (~) of a vector {} equals {}".format(v_0, ~v_0), end="")
    assert isinstance(~v_0, float) and ~v_0 == 6.25,\
        "WRONG: vector length calculation (~ operator)"
    passed()


def testing_operator_unarry_minus():
    """unary minus operator (-)"""
    v_0 = Vector(-2., 4., -7.)
    print("  Unary minus (-) of a {} equals {}".format(v_0, -v_0), end="")
    assert -v_0 == Vector(2., -4., 7.), "WRONG: vector unary minus operator"
    assert -Vector(-0., 0., -7.) == Vector(0., 0., 7.),\
        "WRONG: vector unary minus operator with zero"
    passed()


def testing_operator_sum():
    """operator +"""
    v_1 = Vector(1., -2., 3.)
    v_2 = Vector(2., 3., 4.)
    v_0 = v_1 + v_2
    print("  Sum (+) of {} and {} equals {}".format(v_1, v_2, v_0), end="")
    assert v_0 == Vector(3., 1., 7.), "WRONG: vector sum operator (+)"
    passed()
    print("  Sum of a vector with right operand of a wrong type", end="")
    try:
        v_10 = v_1 + 1.1
        print(v_10)
        assert False, "WRONG: no exception of vector sum with a wrong type"
    except TypeError:
        print(" raises an exception TypeError", end="")
        passed()


def testing_operator_sum_equal():
    """operator +="""
    v_0 = Vector(1., -2., 3.)
    v_2 = Vector(2., 3., 4.)
    vid_1 = v_0
    v_0 += v_2
    vid_2 = v_0
    print("  Adding (+=) of {} to {} "
          "equals {}".format(Vector(1., -2., 3.), v_2, v_0), end="")
    assert v_0 == Vector(3., 1., 7.), "WRONG: vector adding operator (+=)"
    passed()
    print("  The operator (+=) changes ID of the result", end="")
    assert vid_1 != vid_2,\
        "WRONG: vector ID is not changed by (+=) operator"
    passed()


def testing_operator_substraction():
    """binary operator -"""
    v_1 = Vector(1., -2., 3.)
    v_2 = Vector(2., 5., 7.)
    v_0 = v_1 - v_2
    print("  Difference (-) of {} and {} equals {}".format(v_1, v_2, v_0),
          end="")
    assert v_0 == Vector(-1., -7., -4.), "WRONG vector difference operator (-)"
    passed()

    print("  Vector difference operator (-) with a wrong type operand", end="")
    try:
        var = v_1 - 1.1
        print(var)
        assert False,\
            "WRONG: no exception of vector difference (-) with a wrong type"
    except TypeError:
        print(" raises exception TypeError", end="")
        passed()


def testing_operator_substraction_equal():
    """operator -="""
    v_0 = Vector(1., -2., 3.)
    v_2 = Vector(2., 3., 4.)
    vid_1 = v_0
    v_0 -= v_2
    vid_2 = v_0
    print("  Subtraction (-=) {} from a {} "
          "equals {}".format(v_2, Vector(1., -2., 3.), v_0), end="")
    assert v_0 == Vector(-1., -5., -1.),\
        "WRONG vector Subtraction operator (-=)"
    passed()
    print("  The -= operator changes ID of the result", end="")
    assert vid_1 != vid_2, "WRONG: vector ID is not changed by (-=) operator"
    passed()


def testing_operator_multiplication():
    """operator (*) means cross-product or scaling"""

    # operator * with a vector type of a right operand means cross-product
    v_1 = Vector(3., 4., 0.)
    v_2 = Vector(-4., 3., 0.)
    v_0 = Vector()
    v_0 = v_1 * v_2
    print("  Cross-product (*) of {} and {} equals {}".format(v_1, v_2, v_0),
          end="")
    assert v_0 == Vector(0., 0., 25.),\
        "WRONG vector multiplication operator (*) for z-axis result"
    passed()
    v_1 = Vector(0., 3., 4.)
    v_2 = Vector(0., -4., 3.)
    print("  Cross-product (*) of {} and {} "
          "equals {}".format(v_1, v_2, v_1*v_2), end="")
    assert v_1 * v_2 == Vector(25., 0., 0.),\
        "WRONG vector multiplication operator (*) for x-axis result"
    passed()
    v_1 = Vector(3., 0., 4.)
    v_2 = Vector(4., 0., -3.)
    print("  Cross-product (*) of {} and {} "
          "equals {}".format(v_1, v_2, v_1*v_2), end="")
    assert v_1 * v_2 == Vector(0., 25., 0.),\
        "WRONG vector multiplication operator (*) for y-axis result"
    passed()

    # operator * with a number means scaling
    v_1 = Vector(5., -1., 0.1)
    scale = 2.
    v_0 = v_1 * scale
    print("  Multiplication (*) of a {} on a float value {} "
          "equals {}".format(v_1, scale, v_0), end="")
    assert v_0 == Vector(10., -2., 0.2),\
        "WRONG vector multiplication operator (*) with a real number"
    passed()
    v_1 = Vector(5., -1., 0.1)
    scale = 2
    v_0 = v_1 * scale
    print("  Multiplication (*) of a {} on an integer number {} "
          "equals {}".format(v_1, scale, v_0), end="")
    assert v_0 == Vector(10., -2., 0.2),\
        "WRONG multiplication operator (*) of a vector on a real number"
    passed()
    v_1 = Vector(5., -1., 0.1)
    scale = "2w"
    try:
        v_0 = v_1 * scale
        assert False,\
            "WRONG: no exception of multiplication (*) of a vector "\
            "on a wrong type operand"
    except ValueError:
        print("  Multiplication (*) of a vector on a wrong type operand "
              "rises exception ValueError", end="")
    passed()


def testing_operator_and():
    """operator & (dot-product)"""
    v_1 = Vector(1., -2., -3.)
    v_2 = Vector(4., -5., 6.)
    v_0 = v_1 & v_2
    print("  Dot-product (&) of a {} on a {} equals {}".format(v_1, v_2, v_0),
          end="")
    assert v_0 == -4. and isinstance(v_0, float),\
        "WRONG dot-product operator (&) of two vectors"
    assert (not Vector(1., 0., 0.) & Vector(0., 2., 3.) and
            not Vector(0., 2., 0.) & Vector(1., 0., 3.) and
            not Vector(0., 0., 3.) & Vector(1., 2., 0.) and
            not Vector(0., 2., 3.) & Vector(1., 0., 0.) and
            not Vector(1., 0., 3.) & Vector(0., 2., 0.) and
            not Vector(1., 2., 0.) & Vector(0., 0., 3.)),\
        "WRONG dot-product of two vectors by coordinates"
    passed()
    try:
        v_0 = v_1 & "2"
        assert False,\
            "WRONG: no exception of dot-product operator (&) of a vector "\
            "with a wrong operand type"
    except TypeError:
        print("  Dot-product (&) of a vector on an non-vector raises "
              "exception TypeError", end="")
        passed()


def testing_operator_or():
    """operator (|) realizes element-wise multiplication"""
    v_1 = Vector(1., -2., -3.)
    v_2 = Vector(4., -5., 6.)
    v_0 = v_1 | v_2
    print("  Element-wise multiplication (|) of a {} on a {}\n"
          "    equals {}".format(v_1, v_2, v_0), end="")
    assert v_0 == Vector(4., 10., -18.) and isinstance(v_0, Vector),\
        "WRONG element-wise multiplication (operator |) of two vectors"
    assert (not ~(Vector(1., 0., 0.) | Vector(0., 2., 3.)) and
            not ~(Vector(0., 2., 0.) | Vector(1., 0., 3.)) and
            not ~(Vector(0., 0., 3.) | Vector(1., 2., 0.)) and
            not ~(Vector(0., 2., 3.) | Vector(1., 0., 0.)) and
            not ~(Vector(1., 0., 3.) | Vector(0., 2., 0.)) and
            not ~(Vector(1., 2., 0.) | Vector(0., 0., 3.))),\
        "WRONG element-wise multiplication by coordinates"
    passed()
    try:
        v_0 = v_1 | "2"
        assert False,\
            "WRONG: no exception of a Vector operator (|) "\
            "with on a wrong operand type"
    except TypeError:
        print("  Vector operator (|) with an right operand wrong type "
              "rises exception TypeError", end="")
        passed()

def testing_vector_division_operator():
    """Testing of a division operator (/)"""

    v_1 = Vector(1., 0., -3.)
    scalar = 10.
    v_2 = v_1 / scalar
    print("  Division (/) of a vector{} on a float {}"
          " equals {}".format(v_1, scalar, v_2), end="")
    assert v_2 == Vector(0.1, 0., -0.3),\
        "WRONG division operator (/) of a vector on a float value"
    passed()

    print("  Division (/) of vector on a wrong type value ", end="")
    try:
        v_3 = v_1 / v_2
        print(v_3)
        assert False, "Right operand of vector division (/)"\
                      " must raise an exception"
    except TypeError:
        print("raises an exception TypeError", end="")
    passed()


def testing_vector_algebraic_properties():
    """Testing of vector for algebraic properties"""
    print("\nAlgebraic properties of vector operations")

    # cross-product algebraic properties
    v_1 = Vector(1., 2., 3.)
    v_2 = Vector(4., 5., 6.)
    v_3 = Vector(7., 8., 9.)
    print("  Self cross-product equals zero: ~(v_1*v_1) == ",
          ~(v_1 * v_1), end="")
    assert ~(v_1 * v_1) == 0., "WRONG: Self cross-product must be zero"
    passed()
    print("  Cross-product is anti-commutative: v_1*v_2 == -v_2*v_1 ", end="")
    assert v_1 * v_2 == -v_2 * v_1,\
        "WRONG: Cross-product must be anti-commutative"
    passed()
    print("  Cross-product is distributive: v_1*(v_2+v_3) == v_1*v_2+v_1*v_3,",
          " (v_2+v_3)*v_1 == v_2*v_1+v_3*v_1", end="")
    assert (v_1 * (v_2 + v_3) == v_1 * v_2 + v_1 * v_3 and
            (v_2 + v_3) * v_1 == v_2 * v_1 + v_3 * v_1),\
        "WRONG: Cross-product must be distributive"
    passed()
    scalar = 7.
    print("  Cross-product is compatible with scalar multiplication\n",
          "    (v_1 * scalar) * v_2 == v_1 * (v_2 * scalar) =="
          " (v_1 * v_2) * scalar:", end="")
    assert ((v_1 * scalar) * v_2 ==
            v_1 * (v_2 * scalar) ==
            (v_1 * v_2) * scalar),\
        "WRONG: Cross-product must be compatible with scalar multiplication"
    passed()
    print("  Cross-product is non-associative (Jacobi identity):\n",
          "    ~(v_1*(v_2*v_3) + v_2*(v_3*v_1) + v_3*(v_1*v_2))== 0.", end="")
    assert ~(v_1 * (v_2 * v_3) + v_2 * (v_3 * v_1) + v_3 * (v_1 * v_2)) == 0.,\
        "WRONG: Cross-product must satisfy Jacobi identity"
    passed()

    # dot-product algebraic properties
    v_1 = Vector(1., 2., 3.)
    v_2 = Vector(4., 5., 6.)
    v_3 = Vector(7., 8., 9.)
    print("  Self dot-product equals to the squared vector length: "
          "(v_1&v_1) == (~v_1)**2 ", end="")
    assert (v_1 & v_1) == (~v_1)**2,\
        "WRONG: Self dot-product must equal to the squared vector length"
    passed()
    print("  Dot-product is commutative: v_1 & v_2 == v_2 & v_1 ", end="")
    assert v_1 * v_2 == v_1 * v_2, "WRONG dot-product must be commutative"
    passed()
    print("  Dot-product is distributive: v_1 & (v_2 + v_3) =="
          " (v_1 & v_2) + (v_1 & v_3)", end="")
    assert v_1 & (v_2 + v_3) == (v_1 & v_2) + (v_1 & v_3),\
        "WRONG dot-product must be distributive"
    passed()
    scalar1 = 7.
    scalar2 = 8.
    print("  Dot-product is compatible with scalar multiplication:\n",
          "    (v_1 * scalar1) & (v_2 * scalar2) =="
          " (v_1 & v_2) * (scalar1 * scalar2)", end="")
    assert ((v_1 * scalar1) & (v_2 * scalar2) ==
            (v_1 & v_2) * (scalar1 * scalar2)),\
        "WRONG: dot-product must be compatible with scalar multiplication"
    passed()

    # Lagrange identity
    v_1 = Vector(1., 2., 3.)
    v_2 = Vector(4., 5., 6.)
    v_3 = Vector(7., 8., 9.)
    print("  Lagrange identity: v_1 * (v_2 * v_3) =="
          " v_2 * (v_1 & v_3) - v_3 * (v_1 & v_2)", end="")
    assert v_1 * (v_2 * v_3) == v_2 * (v_1 & v_3) - v_3 * (v_1 & v_2),\
        "WRONG: Lagrange identity is not satisfied"
    passed()


def testing_of_class_vector():
    """Testing of class Vector"""
    print('\n\nTESTING OF CLASS "Vector"\n')
    testing_vector_initialization()
    testing_vector_indexing()
    testing_vector_coordinates_access()
    testing_vector_comparison_operators()
    testing_vector_mathematical_operators()
    testing_vector_algebraic_properties()


if __name__ == '__main__':
    testing_of_class_vector()
