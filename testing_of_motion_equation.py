"""
The module contains functions, which test the equation solution of
point particle motion realized in class MotionEquationOfParticle.
There are following particle motion test procedures:
- testing of particle motion under zero force
- testing of particle motion under force with power dependence on time
- testing of particle motion under force with linear dependence on velocity
- testing of particle oscillation
The function testing_motion_equation() runs all of those tests
"""


import math
from dynamics_vector import Vector
from dynamics_of_point_particle import StateParametersOfParticle
from dynamics_of_point_particle import MotionEquationOfMultipleParticles
from testing_of_motion_equation_basic import FunctionalForce
from testing_of_motion_equation_basic import test_motion_equation_solution
from testing_of_motion_equation_basic import find_max_position_velosity_error
from testing_of_motion_equation_basic import print_error_difference_table


def passed():
    """Marks passed tests"""
    print(" =>PASSED")


def testing_zero_force_zero_velocity():
    """
    Testing motion equation solution with zero force with zero velocity
    """
    print("\nTesting of zero force with zero initial velocity")
    # State parameter object with initial conditions for motion equations
    initial_state = StateParametersOfParticle()
    initial_state.time = 0.
    initial_state.position = Vector(10., 20, 30)
    initial_state.velocity = Vector()
    # Including of a force source with zero coordinates of force (by default)
    initial_state.include_source(FunctionalForce())
    initial_state.update()
    print("  Force:", initial_state.force)
    # List of tested takts
    takts = [0.005*(10**i) for i in range(0, 3)]

    # Function of expected position vector calculation
    def pos_expect(current_time, init_state):
        return init_state.position + init_state.velocity * current_time

    # Function of expected velocity vector calculation
    def vel_expect(current_time, init_state):
        return init_state.velocity + init_state.acceleration * current_time

    errors = test_motion_equation_solution(
        initial_state, 5., pos_expect, vel_expect, takts)
    max_pos_error, max_vel_error = find_max_position_velosity_error(errors)

    print("  In case of zero force and zero initial velocity:")
    print("  - the velocity is constant zero: "
          "max error={: 1.0e}".format(max_vel_error))
    print("  - the position is not changed with time: "
          "position = initial_position \n"
          "      max positiion error={: 1.0e}".format(max_pos_error))
    floating_point_error = 1e-10
    assert max_pos_error < floating_point_error and\
        max_vel_error < floating_point_error,\
        "WRONG: calculation error is greater then defined floating-point error"
    print("  The errors are in range of floating-point error", end="")
    passed()


def testing_zero_force_nonzero_velocity():
    """
    Testing motion equation solution with zero force with non-zero velocity
    """
    print("\nTesting of zero force with non-zero initial velocity")
    # State parameter object with initial conditions for motion equations
    initial_state = StateParametersOfParticle()
    initial_state.time = 0.
    initial_state.position = Vector(10., 20, 30)
    initial_state.velocity = Vector(1., 2., 3.)
    # Including of a force source with zero coordinates of force (by default)
    initial_state.include_source(FunctionalForce())
    initial_state.update()
    print("  Force:", initial_state.force)
    # List of tested takts
    takts = [0.005*(10**i) for i in range(0, 3)]

    # Function of expected position vector calculation
    def pos_expect(current_time, init_state):
        return init_state.position + init_state.velocity * current_time

    # Function of expected velocity vector calculation
    def vel_expect(current_time, init_state):
        return init_state.velocity + init_state.acceleration * current_time

    errors = test_motion_equation_solution(
        initial_state, 5., pos_expect, vel_expect, takts)
    max_pos_error, max_vel_error = find_max_position_velosity_error(errors)

    print("  In case of zero force and non-zero velocity:")
    print("  - the velocity is constant: max error=", max_vel_error)
    print("  - the position is changed linearly on time "
          "according to velocity:\n"
          "      position = initial_position + velosity * time\n"
          "      max positiion error={: 1.0e}".format(max_pos_error))
    floating_point_error = 1e-10
    assert max_pos_error < floating_point_error and\
        max_vel_error < floating_point_error,\
        "WRONG: calculation error is greater then defined floating-point error"
    print("  The errors are in range of floating-point error", end="")
    passed()


def testing_force_power_time(power=0):
    """
    Testing motion equation solution with force, which depends on time
    according a power low
    """
    print("\nTesting of force with power dependence on time: "
          "F = k*time**", power)
    # force multipliers for different coordinates:
    # F[i] = force_k[i] * time**power
    force_k = Vector(1., 10., 100.)
    # State parameter object with initial conditions for motion equations
    initial_state = StateParametersOfParticle()
    initial_state.time = 0.
    initial_state.position = Vector()
    initial_state.velocity = Vector()
    # Including of a force source with dependence of force coordinates on time
    initial_state.include_source(FunctionalForce(
        lambda t, p, v: force_k[0] * (t**power),
        lambda t, p, v: force_k[1] * (t**power),
        lambda t, p, v: force_k[2] * (t**power)))
    initial_state.update()
    print("  Force = {} * time**{: .0f}".format(force_k, power))
    # List of tested takts
    takts = [0.005*(10**i) for i in range(0, 3)]

    # Function of expected position vector calculation
    def pos_expect(current_time, init_state):
        return init_state.position +\
               force_k * current_time**(power+2)/((power+1.)*(power+2.))

    # Function of expected velocity vector calculation
    def vel_expect(current_time, init_state):
        return init_state.velocity +\
               force_k * current_time**(power+1)/(power+1.)

    errors = test_motion_equation_solution(
        initial_state, 5., pos_expect, vel_expect, takts)
    max_pos_error, max_vel_error = find_max_position_velosity_error(errors)

    # Result analysis
    floating_point_error = 1e-9
    if power in (0, 1, 2):
        print("  In case of power ==", power)
        if power == 0:
            print("  the force reminds constant in time.")
            print("  - the velocity linearly increases in time: "
                  "velocity = force_k * time")
        else:
            print("  - the velocity increases in time with "
                  "power", power+1, ": velocity = force_k * "
                  "time**{: .0f}/{: .0f}".format(power+1, power+1))

        print("      max error={: .0e}".format(max_vel_error))
        print("  - the position increases in time with power", power+2,
              ": position = force_k * "
              "time**{: .0f}/{: .0f}".format(power+2, (power+1)*(power+2)))
        print("      max error={: .0e}".format(max_pos_error))
        print("  The Runge-Kutta method does not have calculation error "
              "independently on the takt value in this case.")
        print("  The computation position and velocity error theoretically "
              "must be zero.\n  In practice, the computation "
              "error limited by floating-point error.")
        assert max_pos_error < floating_point_error and\
            max_vel_error < floating_point_error,\
            "WRONG: calculation error is greater then defined "\
            "floating-point error in test of force with power dependence"
        print("  The errors are in range of floating-point error", end="")
        passed()
    elif power == 3:
        print("  In case of power == 3")
        print("  - The velocity increases in time with "
              "power", power+1, ": velocity = force_k * "
              "time**{: .0f}/{: .0f}".format(power+1, power+1),
              "\n      max error={: .0e}".format(max_vel_error))
        print("    Theoretically, The Runge-Kutta method has no error "
              "for velocity calculation\n    independently "
              "on the takt value in this case.")
        print("    The computation velocity error theoretically "
              "must be zero.\n    In practice, the computation velocity "
              "error limited by floating-point error.", end="")
        assert max_vel_error < floating_point_error,\
            "WRONG: computation velocity error is greater then defined "\
            "floating-point error in test of force with 4 power dependence"
        passed()
        print("  - The position increases in time with power", power+2,
              ": position = force_k * "
              "time**{: .0f}/{: .0f}".format(power+2, (power+1)*(power+2)),
              "\n      max error={: .0e}".format(max_pos_error))
        print("    The Runge-Kutta method has theoretical calculation position"
              " error\n    which depends on the takt and force_k:"
              "   error = -iteration_number * force_k * takt**5 /120.")
        print("    Differences between this theoretical errors and achieved "
              "computation errors are presented\n    in the table below "
              "for different tack and force_k (denoted k):")
        errors_pos = {key: value[0] for (key, value) in errors.items()}

        def vel_method_error(time, takt):
            return -force_k * time * takt**4 / 120.
        max_error = print_error_difference_table(
            5., force_k, takts, errors_pos, vel_method_error)
        assert max_error < floating_point_error,\
            "WRONG: calculation position error is much greater "\
            "then theoretical error in test of force with 3th power "\
            "dependence on time"
        print("    Maximal difference is {:.1e} ".format(max_error),
              "and is in range of the floating-point error", end="")
        # Stopted
        passed()


def testing_force_on_velocity():
    """
    Testing motion equation solution with force, which linearly depends on
    velocity
    """
    print("\nTesting of force with linear dependence on velocity: "
          "F = kv * vel")
    # velocity coefficient for different coordinates
    vel_k = Vector(0.25, 0.5, 1.)
    # State parameter object with initial conditions for motion equations
    initial_state = StateParametersOfParticle()
    initial_state.time = 0.
    initial_state.position = Vector()
    initial_state.velocity = Vector(1., 1., 1.)
    # Including of a force source with function dependence of force coordinates
    # on velocity
    initial_state.include_source(FunctionalForce(
        lambda t, p, v: vel_k[0] * v[0],
        lambda t, p, v: vel_k[1] * v[1],
        lambda t, p, v: vel_k[2] * v[2]))
    initial_state.update()
    print("  Force = {} * velocity".format(vel_k))
    # List of tested takts
    takts = [0.005*(10**i) for i in range(0, 3)]

    # Function of expected position vector calculation
    def pos_expect(time, init_state):
        res = Vector(init_state.velocity)
        for i in range(3):
            res[i] *= (math.exp(time * vel_k[i]) - 1.) / vel_k[i]
        return res

    # Function of expected velocity vector calculation
    def vel_expect(time, init_state):
        res = Vector(init_state.velocity)
        for i in range(3):
            res[i] *= math.exp(time * vel_k[i])
        return res

    errors = test_motion_equation_solution(
        initial_state, 5., pos_expect, vel_expect, takts)

    # Result analysis
    floating_point_error = 1e-9
    print("  In case of linear positive dependence of force on velocity,"
          " the velocity and position\n  are changed exponentially. "
          "The analytical solution is:\n"
          "    velocity = initial_velocity * exp(kv time)\n"
          "    position_AS = initial_position "
          "+ initial_velocity * (exp(kv time) - 1.) / kv")
    print("  Achieved results deviate in comparison with the analytical\n"
          "    solution with error, which depends on takt and kv.")
    print("  Theoretically, the result of Kunge-Kutta method for "
          "such equation differs\n    from the analytical solution "
          "and depends on takt and kv:\n"
          "    velocity_RK = initial_velocity * (1. + base)**(time/takt)\n"
          "    position_RK = (initial_velocity / kv) * "
          "((1. + base)**(time/takt) - 1.)\n"
          "    where base = kdt * (1. + kdt/2. + kdt**2/6. + kdt**3/24.);  "
          " kdt = takt * kv")
    print("  This creates an theoretical Runge-Kutta calculation error "
          "for position and velocity.")

    # Functions for Runge-Kutta error computation

    def base_rk(time, takt, k_vel):
        """ Computation of base expression for position_RK and velocity_RK"""
        kdt = takt * k_vel
        base = kdt * (1. + kdt/2. + kdt**2/6. + kdt**3/24.)
        return (1. + base)**(time/takt)

    def pos_method_error(time, takt):
        """Computation of Runge_Kutta method error for position"""
        velocity = initial_state.velocity
        pos_as = Vector()
        for coord in range(3):
            pos_as[coord] = (velocity[coord] / vel_k[coord]) *\
                            (math.exp(time * vel_k[coord]) - 1.)
        pos_rk = Vector()
        for coord in range(3):
            pos_rk[coord] = (velocity[coord] / vel_k[coord]) *\
                            (base_rk(time, takt, vel_k[coord]) - 1.)
        return pos_rk - pos_as

    def vel_method_error(time, takt):
        """Computation of Runge_Kutta method error for velocity"""
        velocity = initial_state.velocity
        vel_as = Vector()
        for coord in range(3):
            vel_as[coord] = velocity[coord] * math.exp(time * vel_k[coord])
        vel_rk = Vector()
        for coord in range(3):
            vel_rk[coord] = velocity[coord] *\
                            base_rk(time, takt, vel_k[coord])
        return vel_rk - vel_as

    # Analisys of position error
    print("  - Theoretical Runge-Kutta method position error equals: "
          "    position_RK - position_AS\n"
          "    Difference between the computed position errors and "
          "the theoretical method position error\n"
          "    are presented in the table for different takt and kv:")
    errors_pos = {key: value[0] for (key, value) in errors.items()}
    max_error = print_error_difference_table(
        5., vel_k, takts, errors_pos, pos_method_error)
    assert max_error < floating_point_error,\
        "WRONG: calculation position error is much greater "\
        "then theoretical error in test of force" \
        " with linear velocity dependence"
    print("    Maximal difference is {:.1e} ".format(max_error),
          "and is in range of the floating-point error", end="")
    passed()
    # Analysis of velocity error
    print("  - Theoretical Runge-Kutta method position error equals: "
          "    velocity_RK - velocity_AS\n"
          "    Difference between the computed velocity errors and "
          "the theoretical method velocity error\n"
          "    are presented in the table for different takt and kv:")
    errors_vel = {key: value[1] for (key, value) in errors.items()}
    max_error = print_error_difference_table(
        5., vel_k, takts, errors_vel, vel_method_error)
    assert max_error < floating_point_error,\
        "WRONG: calculation velocity error is much greater "\
        "then theoretical error in test of force" \
        " with linear velocity dependence"
    print("    Maximal difference is {:.1e} ".format(max_error), end="")
    print("and is in range of the floating-point error", end="")
    passed()


def testing_oscillation():
    """Testing motion equation solution of oscillator"""
    print("\nTesting oscillator: F = -kp**2 * pos")
    # position coefficient for different coordinates
    pos_k = Vector(0.25, 0.5, 1.)
    # State parameter object with initial conditions for motion equations
    initial_state = StateParametersOfParticle()
    initial_state.time = 0.
    initial_state.position = Vector(10., 10., 10.)
    initial_state.velocity = Vector()
    # Including of a force source with function dependence of force coordinates
    # on position
    initial_state.include_source(FunctionalForce(
        lambda t, p, v: -pos_k[0]**2 * p[0],
        lambda t, p, v: -pos_k[1]**2 * p[1],
        lambda t, p, v: -pos_k[2]**2 * p[2]))
    initial_state.update()
    print("  Force = -{} * position".format(pos_k))
    # List of tested takts
    takts = [0.005*(10**i) for i in range(0, 3)]

    # Function of expected position vector calculation
    def pos_expect(time, init_state):
        res = Vector()
        for i in range(3):
            res[i] = init_state.position[i] * math.cos(time * pos_k[i])
        return res
    # Function of expected velocity vector calculation

    def vel_expect(time, init_state):
        res = -init_state.position | pos_k
        for i in range(3):
            res[i] *= math.sin(time * pos_k[i])
        return res

    errors = test_motion_equation_solution(
        initial_state, 5., pos_expect, vel_expect, takts)
    # Result analysis
    floating_point_error = 1e-9
    print("  In case of linear negative dependence of force on position,"
          " the velocity and position\n  are changed sinusoidally. "
          "The analytical solution is:\n"
          "    position_AS = initial_position * cos(kp * time)\n"
          "    velosity_AS = -initial_position * sin(kp * time) * kp")
    print("  Achieved results deviate in comparison to the analytical\n"
          "    solution with error, which depends on takt and kp.")
    print("  Theoretically, the result of Kunge-Kutta method for "
          "such equation differs\n    from the analytical solution "
          "and depends on takt and kv:\n"
          "    position_RK = A**(time/takt) * initial_position * "
          "cos(K * time)\n"
          "    velocity_RK = -kp* A**(time/takt) * initial_position * "
          "sin(K * time)\n"
          "    where A = 1 - (kdt)**6 / 9 + (kdt)**8 / 36;  "
          " kdt = takt * kv/sqrt(2)\n"
          "      K = arctang(k * takt * (1-kdt**2 / 3) / "
          "(1 + kdt**2 + kdt**4 / 6)) / tack")
    print("  This creates an theoretical Runge-Kutta calculation error "
          "for position and velocity.")

    # Functions for Runge-Kutta error computation

    def a_rk(time, takt, k_pos):
        """ Computation of amplitude for position_RK and velocity_RK"""
        kdt = (takt * k_pos)**2 / 2.
        ampl = math.sqrt(1. - kdt**3 / 9. + kdt**4 / 36.)
        return ampl**(time / takt)

    def k_rk(takt, k_pos):
        """ Computation of K-value for position_RK and velocity_RK"""
        kdt = (takt * k_pos)**2 / 2.
        base1 = 1. - kdt + kdt**2 / 6.
        base2 = k_pos * takt * (1. - kdt / 3.)
        return math.atan2(base2, base1) / takt

    def pos_method_error(time, takt):
        """Computation of Runge-Kutta method error for position"""
        position = initial_state.position
        pos_as = Vector()
        for coord in range(3):
            pos_as[coord] = position[coord] * math.cos(pos_k[coord] * time)
        pos_rk = Vector()
        for coord in range(3):
            pos_rk[coord] = position[coord] *\
                            a_rk(time, takt, pos_k[coord]) *\
                            math.cos(k_rk(takt, pos_k[coord]) * time)
        return pos_rk - pos_as

    def vel_method_error(time, takt):
        """Computation of Runge-Kutta method error for velocity"""
        position = initial_state.position
        vel_as = Vector()
        for coord in range(3):
            vel_as[coord] = -position[coord] *\
                            math.sin(pos_k[coord] * time) *\
                            pos_k[coord]
        vel_rk = Vector()
        for coord in range(3):
            vel_rk[coord] = -position[coord] *\
                            a_rk(time, takt, pos_k[coord]) *\
                            math.sin(k_rk(takt, pos_k[coord]) * time) *\
                            pos_k[coord]
        return vel_rk - vel_as

    # Analysis of position error
    print("  - Theoretical Runge-Kutta method position error equals: "
          "    position_RK - position_AS\n"
          "    Difference between the computed position errors and "
          "the theoretical method position error\n"
          "    are presented in the table for different takt and kp:")
    # printout the table
    errors_pos = {key: value[0] for (key, value) in errors.items()}
    max_error = print_error_difference_table(
        5., pos_k, takts, errors_pos, pos_method_error)
    assert max_error < floating_point_error,\
        "WRONG: calculation position error is much greater "\
        "then theoretical error in the oscillator test"
    print("    Maximal difference is {:.1e} ".format(max_error),
          "and is in range of the floating-point error", end="")
    passed()
    # Analysis of velocity error
    print("  - Theoretical Runge-Kutta method velocity error equals: "
          "    velocity_RK - velocity_AS\n"
          "    Difference between the computed velocity errors and "
          "the theoretical method velocity error\n"
          "    are presented in the table for different takt and kp:")
    errors_vel = {key: value[1] for (key, value) in errors.items()}
    max_error = print_error_difference_table(
        5., pos_k, takts, errors_vel, vel_method_error)
    assert max_error < floating_point_error,\
        "WRONG: calculation velocity error is much greater "\
        "then theoretical error in test of force" \
        " with linear velocity dependence"
    print("    Maximal difference is {:.1e} ".format(max_error),
          "and is in range of the floating-point error", end="")
    passed()


def compare_vector_with_error(vector, error):
    """Compares each vector coordinate to be less than specified error"""
    return (abs(vector[0]) < error and
            abs(vector[1]) < error and
            abs(vector[2]) < error)


def testing_equetions_of_multiple_particles():
    """Testing motion equation solution of multiple particles"""
    print("\nTesting multiple particles equation initialization", end="")
    multi_equation = MotionEquationOfMultipleParticles()
    assert multi_equation.takt == 0.01 and\
        multi_equation.coef == [0.01/6., 0.01/3., 0.01/3., 0.01/6.] and\
        multi_equation.steps == [0., 0.01/2., 0.01/2, 0.01],\
        "WRONG initialization of MotionEquationOfMultipleParticles object"
    passed()

    print("Testing multiple particles equation "
          "with different constant forces", end="")
    constant_forces = \
        [Vector(1., 0., 0.), Vector(0., 2., 0.), Vector(0., 0., 3.)]

    states = [StateParametersOfParticle() for _ in constant_forces]
    states[0].include_source(FunctionalForce(
        lambda t, p, v: constant_forces[0].x,
        lambda t, p, v: constant_forces[0].y,
        lambda t, p, v: constant_forces[0].z))
    states[1].include_source(FunctionalForce(
        lambda t, p, v: constant_forces[1].x,
        lambda t, p, v: constant_forces[1].y,
        lambda t, p, v: constant_forces[1].z))
    states[2].include_source(FunctionalForce(
        lambda t, p, v: constant_forces[2].x,
        lambda t, p, v: constant_forces[2].y,
        lambda t, p, v: constant_forces[2].z))

    tested_time = 5.
    multi_equation.run(tested_time, states)

    floating_point_error = 1e-9
    expected_position = []
    expected_velocity = []
    pos_coef = tested_time**2 / 2.
    for force in constant_forces:
        expected_position.append(force * pos_coef)
        expected_velocity.append(force * tested_time)

    total_assert = True
    for coord, state in enumerate(states):
        total_assert = total_assert and\
            compare_vector_with_error(state.velocity-expected_velocity[coord],
                                      floating_point_error) and\
            compare_vector_with_error(state.position-expected_position[coord],
                                      floating_point_error)

    assert total_assert, "WRONG multi particles motion equation solution"
    passed()


def testing_motion_equation():
    """The method runs separate motion equation solution tests"""
    print("\n\nTESTING MOTION EQUATION SOLUTION")
    testing_zero_force_zero_velocity()
    testing_zero_force_nonzero_velocity()
    testing_force_power_time(0)
    testing_force_power_time(1)
    testing_force_power_time(2)
    testing_force_power_time(3)
    testing_force_on_velocity()
    testing_oscillation()
    testing_equetions_of_multiple_particles()

testing_motion_equation()
