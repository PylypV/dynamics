"""
The module contains an example of classes for visualisation of computed
motion of mutually interacted particles. The visualisation is based on
OpenGL programming interface for tree dimensional rendering
"""
import sys
import time
import xml.etree.ElementTree
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt
from PyQt5.QtOpenGL import QGLWidget
import OpenGL.GL as gl
import OpenGL.GLU as glu
from dynamics_vector import Vector
from dynamics_of_point_particle import StateParametersOfParticle
from dynamics_of_point_particle import ForceSource
from dynamics_of_point_particle import MotionEquationOfMultipleParticles


class InteractedParticleState(StateParametersOfParticle):
    """
    The class of state parameters of a particle, which interacts with other
    particles.
    The class is inherited from class StateParametersOfParticle
    with additional dictionary self.interaction_params, which contains
    names of all InteractedParticleState objects of all mutually interacting
    particles in the system.
    This dictionary is needed for force computation for each particle
    """
    def __init__(self, interaction_params):
        """
        interaction_params is a dictionary with parameters, which correspond
        to mutually interacting particles: a key is
        a StateParametersOfParticle object name; the value is respective
        particle position. The values are needed for interaction force
        computation for each particle. Same dictionary is passed to all
        StateParametersOfParticle objects of these interacted particles.
        Supposed that the particle has a sphere shape and may have elastic
        collision when the sphere of particles overlapped.
        """
        # same structure as in StateParametersOfParticle
        StateParametersOfParticle.__init__(self)
        # unique name of a particle based on amount of created objects
        self.name = "mass" + str(InteractedParticleState.amount)
        InteractedParticleState.amount += 1
        # a dictionary that presents all mutually interacting particles
        # (see docstring)
        self.interaction_params = interaction_params
        # radius of a particle for elastic collision, [meter]
        self.radius = 0.5
        # color of the particle, which will be visualized
        self.color = (0, 0, 0)

    amount = 0  # a class attribute of amount of the created class objects

    def update(self):
        """
        Overriding of update of the basic class InteractedParticleState
        with additional update of element in interaction_params,
        which is related to this particle
        """
        StateParametersOfParticle.update(self)  # basic update
        # updating of position of this particle in the dictionary
        # interaction_params with current position
        self.interaction_params[self.name] = (self.mass, self.position)


class InteractiveGravity(ForceSource):
    """
    The class presents total gravitational force calculation, which acts to
    a moved particle from other moved particles
    """
    def __init__(self, gravitational_constant=16.):
        self.gravitational_constant = gravitational_constant
        ForceSource.__init__(self)

    def update(self, state_parameters):
        """
        Procedure of gravitational force calculation, which acts to
        the particle associated with state_parameters from other particles
        associated with interaction_params
        """
        self.force = Vector()
        # querying of all particles in the system, which are presented in
        # interaction_params as names of particles with those positions
        for name, interaction in state_parameters.interaction_params.items():
            if name == state_parameters.name:
                continue  # no interaction with itself
            distance = state_parameters.position - interaction[1]
            self.force += -distance *\
                state_parameters.mass * interaction[0] *\
                self.gravitational_constant / ((~distance)**3)


class MultiMotionEquationWithPostUpdate(MotionEquationOfMultipleParticles):
    """
    The class provides a motion equation solution of multi-particles based on
    MotionEquationOfMultipleParticles with an additional procedure of
    particle elastic collision treatment
    """
    def __init__(self, takt=0.01):
        MotionEquationOfMultipleParticles.__init__(self, takt)

    def step(self, state_collection):
        """
        Overridden of one step procedure of Runge-Kutta fourth order method
        with adding elastic collision treatment procedure
        """
        # basic procedure of Runge-Kutta fourth order method
        MotionEquationOfMultipleParticles.step(self, state_collection)

        collisions =[]
        num_of_states = len(state_collection)
        for state1_idx in range(num_of_states):
            for state2_idx in range(state1_idx+1,num_of_states):
                distance = state_collection[state2_idx].position -\
                           state_collection[state1_idx].position
                length = ~distance
                if length < (state_collection[state1_idx].radius +
                             state_collection[state2_idx].radius):
                    direction = distance / length
                    collisions.append((state1_idx, state2_idx, length, direction))

        if len(collisions) > 0:
            collisions.sort(key = lambda elem: elem[2])
            for (state1_idx, state2_idx, length, direction) in collisions:
                state_1 = state_collection[state1_idx]
                state_2 = state_collection[state2_idx]
                sum_imp_mod = \
                    (2. / (state_1.mass + state_2.mass)) *\
                    ((state_2.velocity - state_1.velocity) & direction)
                sum_imp = direction * sum_imp_mod
                state_1.velocity += sum_imp * state_2.mass
                state_2.velocity -= sum_imp * state_1.mass


class InteractedParticles():
    """
    The class encapsulates basic parameters for
    motion calculation of gravitationally interacted particles
    """
    def __init__(self, xml_fale_name):
        """
        Initialisation of parameters for motion calculation of
        interacted particles.
        xml_file_name presents an XML file with interacted particles
        """
        # an object of interacted particles motion equation solution
        self.motion = MultiMotionEquationWithPostUpdate(0.01)
        # dictionary represents all particles, which mutually interact
        # and is passed to all InteractedParticleState objects
        inter_distances = dict()
        # reading the state parameters of particles from a XML file
        # getting a list of objects of state parameters
        self.particles = self.read_states_from_xml(
            xml_fale_name, inter_distances)
        # including of gravitational forces to the objects of state parameters
        for state in self.particles:
            state.include_source(InteractiveGravity())

    def read_vector_from_xml_element(self, element, coord=("x", "y", "z")):
        """
        The method performs reading of a vector value from the XML element
        (element), which contains the coordinate attributes x,y,z
        (or specified coordinate attribute names in coord). The method returns
        a vector object with the read coordinates
        """
        result_vector = Vector()
        if element is not None:
            if coord[0] in element.attrib:
                result_vector.x = float(element.attrib[coord[0]])
            if coord[1] in element.attrib:
                result_vector.y = float(element.attrib[coord[1]])
            if coord[2] in element.attrib:
                result_vector.z = float(element.attrib[coord[2]])
        return result_vector

    def read_states_from_xml(self, xml_file_name, inter_distances):
        """
        Reading of state parameters of multiple particles from
        a XML file (xml_file_name). The method returns a list of
        InteractedParticleState objects
        """
        initial_param = []
        root = xml.etree.ElementTree.parse(xml_file_name).getroot()
        if root.tag != "dynamics" or root.tag is None:
            print("\nWRONG xml file for dynamics of interacted particles")
            return None
        if "takt" in root.attrib:
            self.motion.update_coefficients(float(root.attrib["takt"]))
        if "time_scale" in root.attrib:
            self.time_scale = float(root.attrib["time_scale"])
        for element in root:
            if element.tag == "comment":
                print(element.text)
                continue
            if element.tag == "particle":
                state = InteractedParticleState(inter_distances)
                if "name" in element.attrib:
                    state.name = element.attrib["name"]
                if "mass" in element.attrib:
                    state.mass = float(element.attrib["mass"])
                if "radius" in element.attrib:
                    state.radius = float(element.attrib["radius"])
                state.velocity = self.read_vector_from_xml_element(
                    element.find("velocity"))
                state.position = self.read_vector_from_xml_element(
                    element.find("position"))
                color = self.read_vector_from_xml_element(
                    element.find("color"), ("r", "g", "b"))
                state.color = (int(color[0]), int(color[1]), int(color[2]))
                initial_param.append(state)
        return initial_param


class SceneWidget(QGLWidget, InteractedParticles):
    """
    The class implements OpenGL visualization of motion
    of gravitationally interacting particles based on principles
    specified in the class InteractedParticles.
    The OpenGL visualization presented as a widget which can be inserted to
    a window
    """
    def __init__(self, **keys):
        """
        Initialization of a OpenGL rendered scene with interacted particles.
        xml_file_name presents an XML file with interacted particles
        configuration as is specified in the class InteractedParticles
        """
        super().__init__(**keys)  # Cooperative multiple-inheritance

        # converting particles colors from [0, 255] to [0., 1.],
        # which are used in OpenGL
        for particle in self.particles:
            particle.color = (particle.color[0]/256.,
                              particle.color[1]/256.,
                              particle.color[2]/256.)

        # orientation and position of observer camera (POV)
        # relatively the scene coordinate system.
        # POV rotation angels relatively x, y, z axes, deg
        self.observer_rotate = [0., 0., 0.]
        # POV translation relatively the scene coordinate system, units
        self.observer_translate = [0., 0., 0.]
        # light source position in the scene coordinate system, units
        self.light0_position = [0., 0., 1.]

        # self.dim_scale is ratio of the visualized space in the window,
        # which has [-1, 1] frame, to visualised particles space, 1 / miter
        self.dim_scale = 0.05
        # self.time_scale shows how fast particles time runs
        # relatively real time of visualisation
        self.time_scale = 1.5
        # current calculated particles time (model time),
        # takes in to account self.time_scale  sec
        self.model_time = 0.
        # time of visualisation start, sec
        self.start_time = time.time()
        # time of current graphical frame, sec
        self.current_time = 0.
        # time of previous graphical frame, sec
        self.previous_time = self.current_time
        # specified visualisation takt (depends on implementation), sec
        self.takt = 0.030

        # A widget for current information text in left part of the window
        # the widget is set by main window
        self.central_text_widget = False

        # specifies to run test procedure each visualisation frame
        self.to_use_test_procedure = False

    def initializeGL(self):
        """
        This OpenGL function sets required OpenGL states and is called before
        first call of resizeGL() and paintGL(). The function is overridden
        """
        gl.glClearColor(0.9, 0.9, 0.7, 1.)  # background color
        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glEnable(gl.GL_CULL_FACE)
        gl.glFrontFace(gl.GL_CCW)

        # light configuration
        gl.glEnable(gl.GL_LIGHTING)
        gl.glEnable(gl.GL_COLOR_MATERIAL)
        gl.glEnable(gl.GL_LIGHT0)
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, self.light0_position)

    def resizeGL(self, width, height):
        """
        This OpenGL function is called when the window is resized or
        during first showing. The function is overridden
        """
        gl.glViewport(0, 0, width, height)
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        glu.gluPerspective(
            90,  # field of view in degrees
            width/height,  # aspect ratio
            .25,  # near clipping plane
            200,  # far clipping plane
            )
        glu.gluLookAt(
            0., 0., 1.,  # eyepoint
            0., 0., 0.,  # center-of-view
            0., 1., 0.,  # up-vector
            )
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glLoadIdentity()

    def paintGL(self):
        """This OpenGL function renders a scene and is overridden"""
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glPushMatrix()
        self.observer_position()
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, self.light0_position)
        self.paintScene()
        gl.glPopMatrix()
        self.update()

    def observer_position(self):
        """Matrix formation for the POV of observer"""
        gl.glTranslate(self.observer_translate[0],
                       self.observer_translate[1],
                       self.observer_translate[2])
        gl.glRotate(self.observer_rotate[1], 0., 1., 0.)
        gl.glRotate(self.observer_rotate[0], 1., 0., 0.)
        gl.glRotate(self.observer_rotate[2], 0., 0., 1.)

    def sphere(self, radius, color=(0, 0, 1), translate=(0., 0.2, 0.1)):
        """Useful interface for sphere drawing"""
        gl.glPushMatrix()
        gl.glTranslate(translate[0], translate[1], translate[2])
        gl.glColor3f(color[0], color[1], color[2])
        quadric = glu.gluNewQuadric()
        glu.gluSphere(quadric, radius, 10, 10)
        glu.gluDeleteQuadric(quadric)
        gl.glPopMatrix()

    def paintScene(self):
        """
        Motion calculation and drawing of interacted particles in the window
        """
        self.current_time = time.time() - self.start_time
        # calculation of real time between current and
        # previous visualization frame, sec
        real_takt = self.current_time - self.previous_time
        self.previous_time = self.current_time

        # calculation of current model time to be calculated.
        # If the visualisation tact is grater than specified self.takt
        # or if the time-scaled real time grater then current model_time
        # then the self.takt is used
        model_current_tact = self.time_scale *\
            (real_takt
             if (real_takt < self.takt and
                 self.current_time * self.time_scale - self.model_time <= 0.)
             else self.takt)
        self.model_time += model_current_tact

        # solving of the motion equation of all particles
        self.motion.run(self.model_time, self.particles)

        # drawing of particles and those trajectories
        for particle in self.particles:
            self.sphere(particle.radius * self.dim_scale,
                        particle.color,
                        particle.position * self.dim_scale)

        # the text is the information widget in left part of the window
        if self.central_text_widget:
            self.central_text_widget.setText(
                "run time\n% 8.1f sec\n\n" % self.current_time +
                "particles time\n% 8.1f sec\n\n" % self.model_time +
                "graphic tact\n% 8.0f ms\n\n" % (real_takt * 1000.) +
                "modeling tact\n% 8.1f ms\n\n" % (self.motion.takt * 1000.) +
                "time scale\n% 8.1f" % self.time_scale)

        # if there is a test procedure
        if self.to_use_test_procedure:
            self.test_procedure()

    def test_procedure(self):
        """the test procedure runs if self.to_use_test_procedure is True"""
        mark_value = self.particles[0].velocity.x
        mark_time = self.particles[0].time
        if not hasattr(self, '_previous_value'):
            self._previous_value = mark_value
            self._previos_time = False
        # if mark_value >= 0 and self._previous_value < 0.:
        if self._previous_value < 0. < mark_value:
            if self._previos_time:
                print("% 8.3f " % mark_time +
                      "% 8.4f" % (mark_time - self._previos_time))
            self._previos_time = mark_time
        self._previous_value = mark_value


class MainWindow(QMainWindow):
    """The class of a main window with an OpenGL widget"""
    def __init__(self, xml_fale_name):
        """
        Initialization of a QT main window with an OpenGL widget.
        xml_file_name presents an XML file with interacted particles
        configuration as is specified in the class InteractedParticles
        """
        super().__init__()
        self.setWindowTitle("Interacted particles")
        self.setGeometry(50, 50, 500, 500)

        # The central widget of the window (central_widget) has
        # a layout (central_layout), which consists of:
        # - a OpenGL widget (gl_widget of QGLWidget class) with rendered scene
        # - a left bar widget (left_tool_bar) with information text
        central_widget = QWidget()
        self.setCentralWidget(central_widget)
        central_layout = QHBoxLayout()
        central_widget.setLayout(central_layout)
        central_layout.setSpacing(0)
        central_layout.setContentsMargins(0, 0, 0, 0)
        # first including of the left bar widget
        left_tool_bar = QWidget()
        central_layout.addWidget(left_tool_bar)
        # including of the OpenGL widget
        self.gl_widget = SceneWidget(xml_fale_name = xml_fale_name)
        central_layout.addWidget(self.gl_widget)

        # Configuration of the left bar widget
        left_tool_bar.setFixedWidth(100)  # 100 px of the bar width
        # the bar widget has a layout (left_tool_bar_layout), which contains
        # - label text widget (self.gl_widget.central_text_widget with) is
        # passed to OpenGL widget in order to have access for text printing
        left_tool_bar_layout = QVBoxLayout()
        left_tool_bar.setLayout(left_tool_bar_layout)
        self.gl_widget.central_text_widget = QLabel()
        left_tool_bar_layout.addWidget(self.gl_widget.central_text_widget)
        left_tool_bar_layout.addStretch()

    def keyPressEvent(self, event):
        """The inherited method treats the pressed keys"""
        key = event.key()
        text = event.text()
        is_shift_pressed = int(event.modifiers()) & Qt.SHIFT

        if key == Qt.Key_Escape:
            self.close()
        if text == "l":
            self.gl_widget.light0_position[2] += 0.01
        if text == "L":
            self.gl_widget.light0_position[2] -= 0.01
        if text == "m":
            self.gl_widget.time_scale += 0.1

        if not is_shift_pressed:
            if key == Qt.Key_Up:
                self.gl_widget.observer_rotate[0] -= 15.
            if key == Qt.Key_Down:
                self.gl_widget.observer_rotate[0] += 15.
            if key == Qt.Key_Right:
                self.gl_widget.observer_rotate[1] -= 15.
            if key == Qt.Key_Left:
                self.gl_widget.observer_rotate[1] += 15.
            if key == Qt.Key_PageUp:
                self.gl_widget.observer_rotate[2] += 15.
            if key == Qt.Key_PageDown:
                self.gl_widget.observer_rotate[2] -= 15.
        else:
            if key == Qt.Key_PageUp:
                self.gl_widget.dim_scale += 0.01
            if key == Qt.Key_PageDown:
                self.gl_widget.dim_scale -= 0.01

        print(self.gl_widget.observer_rotate,
              self.gl_widget.light0_position,
              self.gl_widget.time_scale)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow('interacting_particles.xml')
    window.gl_widget.to_use_test_procedure = False
    window.show()
    app.exec_()
